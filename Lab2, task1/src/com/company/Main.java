package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int a = 0;
        int p = 0;
        double m1 = 0;
        double m2 = 0;

        Scanner sc = new Scanner(System.in);

        System.out.print("Input a: ");
        a = CheckIntInput(a);

        System.out.print("Input p: ");
        p = CheckIntInput(p);

        System.out.print("Input m1: ");
        m1 = CheckDoubleInput(m1);

        System.out.print("Input m2: ");
        m2 = CheckDoubleInput(m2);

        double sum = m1 + m2;
        if (sum <= 0){
            sum = Math.abs(sum);
        }

        double G = 4 * Math.PI * (Math.pow(a,3)/(Math.pow(p,2)*sum));
        System.out.println("Result = " + G);
    }

    public static int CheckIntInput (int a){
        Scanner sc = new Scanner(System.in);
        boolean check = false;

        while(!check)
        {
            if (sc.hasNextInt())
            {
                a = sc.nextInt();
                System.out.print("You entered: " + a);
                check = true;
            }
            else
            {
                System.out.println("Wrong value, please, repeat!");
                sc.next();
                check = false;
            }
        }
        System.out.println();
        return a;
    }

    public static double CheckDoubleInput (double value){
        Scanner sc = new Scanner(System.in);
        boolean check = false;

        while(!check)
        {
            if (sc.hasNextDouble())
            {
                value = sc.nextDouble();
                System.out.print("You entered: " + value);
                check = true;
            }
            else
            {
                System.out.println("Wrong value, please, repeat!");
                sc.next();
                check = false;
            }
        }
        System.out.println();
        return value;
    }
}
