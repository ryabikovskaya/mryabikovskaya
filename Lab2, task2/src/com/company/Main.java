package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int algorithmld = 0;
        int loopType = 0;
        int n = 0;

        System.out.print("Введите номер типа алгоритма: ");
        algorithmld = Helper.CheckIntInput(algorithmld);
        System.out.print("Введите номер типа цикла: ");
        loopType = Helper.CheckIntInput(loopType);
        System.out.print("Введите параметр, передаваемый в алгоритм: ");
        n = Helper.CheckIntInput(n);

        switch (algorithmld){
            case 1:
                Fibonacci.ChooseLoopType(loopType, n);
                break;
            case 2:
                Factorial.ChooseLoopType(loopType, n);
                break;
        }
    }
}
