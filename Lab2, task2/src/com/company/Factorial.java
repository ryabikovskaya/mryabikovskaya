package com.company;

public class Factorial {

    public static void ChooseLoopType (int loopType, int n){

        switch(loopType){
            case 1:
                CalculateByWhile(n);
                break;
            case 2:
                CalculateByDoWhile(n);
                break;
            case 3:
                CalculateByFor(n);
                break;
        }
    }

    public static int CalculateByWhile(int n){
        int result = 1;
        int i = 1;

        while (i <= n){
            result = result * i;
            i++;
        }

        System.out.print("Факториал введенного числа: " + result);

        return result;
    }

    public static int CalculateByDoWhile(int n){
        int result = 1;
        int i = 1;

        do{
            result = result * i;
            i++;
        }
        while (i <= n);

        System.out.print("Факториал введенного числа: " + result);

        return result;
    }

    public static int CalculateByFor(int n){
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }

        System.out.print("Факториал введенного числа: " + result);

        return result;
    }

}
