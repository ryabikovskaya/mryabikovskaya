package com.company;

public class Fibonacci {

    public static void ChooseLoopType (int loopType, int n){

        switch(loopType){
            case 1:
                CalculateByWhile(n);
                break;
            case 2:
                CalculateByDoWhile(n);
                break;
            case 3:
                CalculateByFor(n);
                break;
        }
    }

    public static int[] CalculateByWhile(int n){
        int[] arr = new int[n];
        arr[0] = 0;
        arr[1] = 1;
        int i = 2;

        while (i < arr.length){
            arr[i] = arr[i - 1] + arr[i - 2];
            i++;
        }

        System.out.print("Ряд чисел Фибоначчи: ");

        for (i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+"  ");
        }

        return arr;
    }

    public static int[] CalculateByDoWhile(int n){
        int[] arr = new int[n];
        arr[0] = 0;
        arr[1] = 1;
        int i = 2;

        do{
            arr[i] = arr[i - 1] + arr[i - 2];
            i++;
        }
        while (i < arr.length);

        System.out.print("Ряд чисел Фибоначчи: ");

        for (i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+"  ");
        }

        return arr;
    }

    public static int[] CalculateByFor(int n){
        int[] arr = new int[n];
        arr[0] = 0;
        arr[1] = 1;
        for (int i = 2; i < arr.length; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }

        System.out.print("Ряд чисел Фибоначчи: ");

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+"  ");
        }
        return arr;
    }

}
