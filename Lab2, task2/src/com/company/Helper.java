package com.company;

import java.util.Scanner;

public class Helper {

    public static int CheckIntInput (int a){
        Scanner sc = new Scanner(System.in);

        boolean check = false;

        while(!check)
        {
            if (sc.hasNextInt())
            {
                a = sc.nextInt();
                check = true;
            }
            else
            {
                System.out.println("Wrong value, please, repeat!");
                sc.next();
                check = false;
            }
        }
        return a;
    }
}
