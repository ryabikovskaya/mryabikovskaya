package tests;

import com.company.Factorial;
import com.company.Fibonacci;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {

    @Test
    void calculateByWhile() {
        int actual = Factorial.CalculateByWhile(5);
        int expected = 120;
        assertEquals(expected, actual);
    }

    @Test
    void calculateByDoWhile() {
        int actual = Factorial.CalculateByDoWhile(5);
        int expected = 120;
        assertEquals(expected, actual);
    }

    @Test
    void calculateByFor() {
        int actual = Factorial.CalculateByFor(5);
        int expected = 120;
        assertEquals(expected, actual);
    }
}