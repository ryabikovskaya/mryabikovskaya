package tests;

import com.company.Fibonacci;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FibonacciTest {

    @Test
    void calculateByWhile() {
        int[] actual = Fibonacci.CalculateByWhile(5);
        int[] expected = new int[]{0, 1, 1, 2, 3};
        assertArrayEquals(expected, actual);
    }

    @Test
    void calculateByDoWhile() {
        int[] actual = Fibonacci.CalculateByDoWhile(5);
        int[] expected = new int[]{0, 1, 1, 2, 3};
        assertArrayEquals(expected, actual);
    }

    @Test
    void calculateByFor() {
        int[] actual = Fibonacci.CalculateByFor(5);
        int[] expected = new int[]{0, 1, 1, 2, 3};
        assertArrayEquals(expected, actual);
    }
}