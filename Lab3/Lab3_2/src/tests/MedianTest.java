package tests;

import com.company.Median;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MedianTest {

    @Test
    public void testIntMedian() {
        double result = Median.FindMedianFromArrayOfInt(new int[] { 5, 5, 5, 100, 5, 5, 5, 5 });
        assertEquals(5, result, 0);
    }

    @Test
    public void testIntMedianOddNumber() {
        double result = Median.FindMedianFromArrayOfInt(new int[] {1, 5, 2, 8, 7});
        assertEquals(5, result, 0);
    }

    @Test
    public void testIntMedianEvenNumber() {
        double result = Median.FindMedianFromArrayOfInt(new int[] {1, 6, 2, 8, 7, 2});
        assertEquals(4, result, 0);
    }

    @Test
    public void testIntMedianEvenAverage() {
        double result = Median.FindMedianFromArrayOfInt(new int[] {1, 2, 3, 4});
        assertEquals(2.5, result, 0);
    }

    @Test
    public void testDoubleMedian() {
        double result = Median.FindMedianFromArrayOfDouble(new double[] { 1, 0.5, 0.5, 0.5, 0.5, 0.55, 0.5, 0.5});
        assertEquals(0.5, result, 0);
    }

    @Test
    public void testDoubleMedianOddNumber() {
        double result = Median.FindMedianFromArrayOfDouble(new double[] { 0.5, 0.2, 0.4, 0.3, 0.1});
        assertEquals(0.3, result, 0);
    }

    @Test
    public void testDoubleMedianEvenAverage() {
        double result = Median.FindMedianFromArrayOfDouble(new double[] { 0.1, 0.2, 0.3, 0.4, 0.2, 0.5});
        assertEquals(0.25, result, 0);
    }

}