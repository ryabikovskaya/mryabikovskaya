package com.company;

import java.util.Arrays;

public class Median {
    //медиана - полусумма 2х элементов посередине массива, при условии, что кол-во чисел четное

    public static double FindMedianFromArrayOfInt(int[] array)
    {
        int[] copiedArray = array.clone();
        Arrays.sort(copiedArray);

        if (copiedArray.length % 2 == 0)
        {
            return ((copiedArray[copiedArray.length / 2] + copiedArray[copiedArray.length / 2 - 1]) / 2f);
        }

        return copiedArray[copiedArray.length / 2];
    }

    public static double FindMedianFromArrayOfDouble(double[] array)
    {
        double[] copiedArray = array.clone();
        Arrays.sort(copiedArray);

        if (copiedArray.length % 2 == 0)
        {
            return ((copiedArray[copiedArray.length / 2] + copiedArray[copiedArray.length / 2 - 1]) / 2f);
        }

        return copiedArray[copiedArray.length / 2];
    }

}
