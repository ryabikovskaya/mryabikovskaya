package tests;

import com.company.Card;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test
    void TestCreateCard() {
        Card expected = new Card("Name",BigDecimal.valueOf(10));
        Card actual = Card.CreateCard("Name", BigDecimal.valueOf(10));
        assertEquals(expected.Balance, actual.Balance);
    }

    @Test
    void TestAddMoneyOnCardBalance() {

        Card expected = new Card("Name",BigDecimal.valueOf(20));
        Card actual = new Card("Name", BigDecimal.valueOf(10));
        Card.ChangeCardBalance(actual, BigDecimal.valueOf(10));
        assertEquals(expected.Balance, actual.Balance);
    }

    @Test
    void TestWithdrawMoneyFromCardBalance() {

        Card expected = new Card("Name",BigDecimal.valueOf(20));
        Card actual = new Card("Name", BigDecimal.valueOf(30));
        Card.ChangeCardBalance(actual, BigDecimal.valueOf(-10));
        assertEquals(expected.Balance, actual.Balance);
    }

    @Test
    void TestCurrencyExchange() {
        Card expected = new Card("Name",BigDecimal.valueOf(2.0));
        Card actual = new Card("Name", BigDecimal.valueOf(5));
        Card.CurrencyExchange(actual, "USD");
        assertEquals(expected.Balance, actual.Balance);
    }
}