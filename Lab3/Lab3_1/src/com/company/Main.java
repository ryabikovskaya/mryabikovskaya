package com.company;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {

        Card newCard = Card.CreateCard("Ivanov", BigDecimal.valueOf(30));
        Card.ShowCardInfo(newCard);

        Card.ChangeCardBalance(newCard, BigDecimal.valueOf(15));
        Card.ShowCardInfo(newCard);

        Card.CurrencyExchange(newCard, "USD");
        Card.ShowCardInfo(newCard);
    }
}
