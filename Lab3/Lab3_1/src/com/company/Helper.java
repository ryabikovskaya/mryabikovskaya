package com.company;

import java.math.BigDecimal;
import java.util.Scanner;

public class Helper {

    public static BigDecimal CheckBigDecimalInput(BigDecimal value) {
        Scanner sc = new Scanner(System.in);
            if (sc.hasNextBigDecimal()) {
                System.out.println("Success!");
                value = sc.nextBigDecimal();
            } else {
                System.out.println("It's not a BigDecimal!");
                //sc.next();
            }
        System.out.println();
        return value;
    }

    public static int CheckIntInput (int value){
        Scanner sc = new Scanner(System.in);
        boolean check = false;

        while(!check)
        {
            if (sc.hasNextInt())
            {
                value = sc.nextInt();
                check = true;
            }
            else
            {
                System.out.println("Wrong value, please, repeat!");
                sc.next();
                check = false;
            }
        }
        System.out.println();
        return value;
    }
}