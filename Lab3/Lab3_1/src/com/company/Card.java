package com.company;

import java.math.BigDecimal;
import java.util.Scanner;

public class Card {

    public String OwnerName;

    public BigDecimal Balance;

    public String Currency = "BYN";

    public Card(String ownerName, BigDecimal balance){
        OwnerName = ownerName;
        Balance = balance;
        Currency = "BYN";
    }

    public Card(String ownerName){
        OwnerName = ownerName;
        Balance = new BigDecimal(0);
        Currency = "BYN";
    }

    public static void ShowCardInfo(Card card){
        System.out.println("Owner Name: " + card.OwnerName + " Balance: " + card.Balance +
                            " Currency: "+ card.Currency);
    }

    public static Card CreateCard(String ownerName, BigDecimal balance){

        Card newCard;
        if (balance == BigDecimal.valueOf(0)){
            newCard = new Card(ownerName);
        }
        else{
            newCard = new Card(ownerName, balance);
        }
        return newCard;
    }

    public static void ChangeCardBalance(Card card, BigDecimal value){

        if (value.intValue() > 0){
            card.Balance = card.Balance.add(value);
        }
        else if (value.intValue() < 0){
            value = value.abs();
            card.Balance = card.Balance.subtract(value);
        }
    }

    public static void CurrencyExchange(Card card, String chosenCurrency){

        switch (chosenCurrency){
            case "USD":
                if (card.Currency != "USD"){
                    card.Balance = card.Balance.multiply(BigDecimal.valueOf(0.4));
                    card.Currency = "USD";
                }
                else
                    System.out.println("Your money is already in USD");
                break;
            case "BYN":
                if (card.Currency != "BYN"){
                    card.Balance = card.Balance.multiply(BigDecimal.valueOf(2.5));
                    card.Currency = "BYN";
                }
                else
                    System.out.println("Your money is already in BYN");
                break;
            default:
                System.out.println("Something went wrong...");
        }
    }
}
